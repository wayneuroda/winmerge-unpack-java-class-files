# Readme
This project creates a DLL "unpacker" plugin for Winmerge.
Specifically it is for running .class files through javap so one can compare functions (eg. for maintaining differences in an API).

The code takes the path to the class file and splits it into the class' path and the class name. `javap.exe` is called with the working directory set to the class' path.

This code was adapted from DisplayBinaryFiles plugin which is available with winmerge source package.

There is a pre-built DLL under the `Prebuilt` folder. If it doesn't work, try installing https://aka.ms/vs/17/release/vc_redist.x86.exe


# Building
Should build out of the box with visual studio 2022 provided you have the right SDKs etc to build windows DLLs.

Open the solution file, then build.


# Installation
>| Note: javap.exe must be on your path. |
>| ------------------------------------- |

Locate your winmerge install folder (`winmergeU.exe` lives here). If there is no `MergePlugins` folder, create one.

Copy the file `UnpackJavaClassFiles.dll` to the `MergePlugins` folder.

Check that plugins are enabled under `Plugins`->`Plugin Settings...`

If you want automatic unpacking make sure you select that in the `Plugins` menu.


# License
Copyright 2023 Wayne Uroda

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
