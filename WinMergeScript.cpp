
#define UNICODE
#include "atl.h"
#include "MIDL/UnpackJavaClassFiles.h"
#include "WinMergeScript.h"

#include <strsafe.h>


STDMETHODIMP CWinMergeScript::get_PluginEvent(BSTR *pVal)
{
	*pVal = SysAllocString(L"FILE_PACK_UNPACK");
	return S_OK;
}


STDMETHODIMP CWinMergeScript::get_PluginDescription(BSTR *pVal)
{
	*pVal = SysAllocString(L"Passes java .class files through javap so that they can be compared in human viewable form - edit/save is not possible.");
	return S_OK;
}


STDMETHODIMP CWinMergeScript::get_PluginFileFilters(BSTR *pVal)
{
	*pVal = SysAllocString(L"\\.class$");
	return S_OK;
}


STDMETHODIMP CWinMergeScript::get_PluginIsAutomatic(VARIANT_BOOL *pVal)
{
	*pVal = VARIANT_TRUE;
	return S_OK;
}


STDMETHODIMP CWinMergeScript::UnpackBufferA(SAFEARRAY **pBuffer, INT *pSize, VARIANT_BOOL *pbChanged, INT *pSubcode, VARIANT_BOOL *pbSuccess)
{
	// We don't need it
	return S_OK;
}


STDMETHODIMP CWinMergeScript::PackBufferA(SAFEARRAY **pBuffer, INT *pSize, VARIANT_BOOL *pbChanged, INT subcode, VARIANT_BOOL *pbSuccess)
{
	// We don't need it
	return S_OK;
}


// Format a readable error message, display a message box
void ErrorExit(PTSTR lpszFunction) 
{
	LPVOID lpMsgBuf;
	LPVOID lpDisplayBuf;
	DWORD dw = GetLastError(); 

	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | 
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		dw,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR) &lpMsgBuf,
		0, NULL );

	lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT, 
		(lstrlen((LPCTSTR)lpMsgBuf)+lstrlen((LPCTSTR)lpszFunction)+40)*sizeof(TCHAR)); 
	StringCchPrintf((LPTSTR)lpDisplayBuf, 
		LocalSize(lpDisplayBuf) / sizeof(TCHAR),
		TEXT("%s failed with error %d: %s"), 
		lpszFunction, dw, lpMsgBuf); 
	MessageBox(NULL, (LPCTSTR)lpDisplayBuf, TEXT("Error"), MB_OK | MB_SYSTEMMODAL);

	LocalFree(lpMsgBuf);
	LocalFree(lpDisplayBuf);
}


// Create a child process that uses the previously created pipes for STDIN and STDOUT.
bool CreateChildProcess(HANDLE stdoutWrite, HANDLE stdinRead, WCHAR *commandLine, WCHAR *workingDirectory)
{ 
	PROCESS_INFORMATION piProcInfo; 
	STARTUPINFO siStartInfo;
	BOOL bSuccess = FALSE; 

	// CreateProcess writes to the command line so allocate an actual buffer for it which is writable
	size_t commandLineBufferSize = wcslen(commandLine) + 1;
	WCHAR *commandLineBuffer = new WCHAR[commandLineBufferSize];
	wcscpy_s(commandLineBuffer, commandLineBufferSize, commandLine);

	// Set up members of the PROCESS_INFORMATION structure. 
	ZeroMemory(&piProcInfo, sizeof(PROCESS_INFORMATION));

	// Set up members of the STARTUPINFO structure. 
	// This structure specifies the STDIN and STDOUT handles for redirection.
	ZeroMemory(&siStartInfo, sizeof(STARTUPINFO));
	siStartInfo.cb = sizeof(STARTUPINFO); 
	siStartInfo.hStdError = stdoutWrite;
	siStartInfo.hStdOutput = stdoutWrite;
	siStartInfo.hStdInput = stdinRead;
	siStartInfo.dwFlags = STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW;
	siStartInfo.wShowWindow = SW_HIDE;

	// Create the process. 
	bSuccess = CreateProcess(NULL, 
		commandLineBuffer,		// command line 
		NULL,					// process security attributes 
		NULL,					// primary thread security attributes 
		TRUE,					// handles are inherited 
		0,						// creation flags 
		NULL,					// use parent's environment 
		workingDirectory,
		&siStartInfo,			// STARTUPINFO pointer 
		&piProcInfo);			// receives PROCESS_INFORMATION 
	
	if (!bSuccess)
	{
		// Close handles to the child process and its primary thread.
		if (piProcInfo.hProcess)
		{
			CloseHandle(piProcInfo.hProcess);
			piProcInfo.hProcess = NULL;
		}
		
		if (piProcInfo.hThread)
		{
			CloseHandle(piProcInfo.hThread);
			piProcInfo.hThread = NULL;
		}
	}

	delete [] commandLineBuffer;

	return bSuccess;
}


// Read output from the child process's pipe for STDOUT
// and write to the file specified 
// Stop when there is no more data. 
void ReadFromPipe(HANDLE stdoutRead, WCHAR *outputFilename) 
{ 
	DWORD dwRead, dwWritten; 
	CHAR chBuf[4096]; 
	BOOL bSuccess = FALSE;

	HANDLE outputFile = CreateFileW(outputFilename, GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

	while (true)
	{
		bSuccess = ReadFile( stdoutRead, chBuf, sizeof(chBuf), &dwRead, NULL);

		if(!bSuccess || dwRead == 0)
		{
			break; 
		}

		bSuccess = WriteFile(outputFile, chBuf, dwRead, &dwWritten, NULL);

		if (!bSuccess )
		{
			break;
		}
	}

	CloseHandle(outputFile);
	outputFile = NULL;
} 


void runTextConverter(WCHAR *workingDir, WCHAR *exe, WCHAR *arguments, WCHAR *outputFile)
{
	HANDLE stdinRead = NULL;
	HANDLE stdinWrite = NULL;
	HANDLE stdoutRead = NULL;
	HANDLE stdoutWrite = NULL;

	// Set the bInheritHandle flag so pipe handles are inherited. 
	SECURITY_ATTRIBUTES saAttr;
	saAttr.nLength = sizeof(SECURITY_ATTRIBUTES);
	saAttr.bInheritHandle = TRUE;
	saAttr.lpSecurityDescriptor = NULL;

	// Create a pipe for the child process's STDOUT.
	if (!CreatePipe(&stdoutRead, &stdoutWrite, &saAttr, 0))
	{
		ErrorExit(TEXT("Stdout CreatePipe"));
	}

	// Ensure the read handle to the pipe for STDOUT is not inherited.
	if (!SetHandleInformation(stdoutRead, HANDLE_FLAG_INHERIT, 0))
	{
		ErrorExit(TEXT("Stdout SetHandleInformation"));
	}

	// Create a pipe for the child process's STDIN.
	if (!CreatePipe(&stdinRead, &stdinWrite, &saAttr, 0))
	{
		ErrorExit(TEXT("Stdin CreatePipe"));
	}

	// Ensure the write handle to the pipe for STDIN is not inherited.
	if (!SetHandleInformation(stdinWrite, HANDLE_FLAG_INHERIT, 0))
	{
		ErrorExit(TEXT("Stdin SetHandleInformation"));
	}

	// Create the child process.
	WCHAR commandLine[32767] = {0};
	StringCchPrintfW(commandLine, 32767, L"%s %s", exe, arguments);
	if (!CreateChildProcess(stdoutWrite, stdinRead, commandLine, workingDir))
	{
		StringCchPrintfW(commandLine, 32767, L"Unable to launch %s %s", exe, arguments);
		ErrorExit(commandLine);
	}

	// Close the pipe handle so the child process stops reading.
	CloseHandle(stdinWrite);
	stdinWrite = NULL;

	CloseHandle(stdoutWrite);
	stdoutWrite = NULL;

	// Read from pipe that is the standard output for child process.
	ReadFromPipe(stdoutRead, outputFile); 

	// close handles
	if (stdoutRead != NULL)
	{
		CloseHandle(stdoutRead);
		stdoutRead = NULL;
	}

	if (stdoutWrite != NULL)
	{
		CloseHandle(stdoutWrite);
		stdoutWrite = NULL;
	}

	if (stdinRead != NULL)
	{
		CloseHandle(stdinRead);
		stdinRead = NULL;
	}

	if (stdinWrite != NULL)
	{
		CloseHandle(stdinWrite);
		stdinWrite = NULL;
	}
} 


void getJavaClassName(WCHAR *className, WCHAR *path, const WCHAR *filename)
{
	int inIdx = 0;
	int outIdx = 0;
	int periodSeen = false;

	int lastBackslash = 0;
	

	// go through className, look for last '\'
	// take all chars until the '.'

	while (filename[inIdx])
	{
		if (filename[inIdx] == '\\')
		{
			// start storing this name
			outIdx = 0;
			periodSeen = false;
			lastBackslash = inIdx;
		}
		else if (filename[inIdx] == '.')
		{
			periodSeen = true;
		}
		else
		{
			if (!periodSeen)
			{
				className[outIdx++] = filename[inIdx];
			}
		}

		inIdx++;
	}

	// Null terminate
	className[outIdx++] = 0;

	// Copy the path
	int i;
	outIdx = 0;
	for (i = 0; i < lastBackslash; i++)
	{
		path[outIdx++] = filename[i];
	}
	path[outIdx++] = 0;
}


// This function is what actually does the translation
// Two filenames are given; fileSrcW and fileDstW
// We pass the Source file to javap and write the results to the temporary dest file
STDMETHODIMP CWinMergeScript::UnpackFile(BSTR fileSrcW, BSTR fileDstW, VARIANT_BOOL *pbChanged, INT *pSubcode, VARIANT_BOOL *pbSuccess)
{
	WCHAR currentClassName[32767];
	WCHAR currentClassPath[32767];
	getJavaClassName(currentClassName, currentClassPath, fileSrcW);

	runTextConverter(currentClassPath, L"javap.exe", currentClassName, fileDstW);

	*pbChanged = VARIANT_TRUE;
	*pbSuccess = VARIANT_TRUE;
	return S_OK;
}


STDMETHODIMP CWinMergeScript::PackFile(BSTR fileSrc, BSTR fileDst, VARIANT_BOOL *pbChanged, INT pSubcode, VARIANT_BOOL *pbSuccess)
{
	// always return error so the users knows we cannot repack
	*pbChanged = VARIANT_FALSE;
	*pbSuccess = VARIANT_FALSE;
	return S_OK;
}
